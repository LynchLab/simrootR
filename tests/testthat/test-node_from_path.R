
context("test getting nodes from paths")

dat = XML::xmlParse("data/straightroot.xml")

test_that("node_from_path returns empty list if no match", {
	expect_s3_class(node_from_path(dat, "fakepath"), "tbl_df")
	expect_length(node_from_path(dat, "fakepath"), 0)
	expect_length(node_from_path(dat, "fakepath/rootDryWeight"), 0)
	expect_length(node_from_path(dat, "plants/simpleRoot/plantPosition/fakenode"), 0)
})

test_that("node_from_path returns path, value, node for each match", {
	paths = c(
		"origin/plants/simpleRoot/plantingTime",
		"plants/simpleRoot/plantingTime",
		"simpleRoot/plantingTime",
		"plantingTime")
	for (p in paths) {
		expect_match(node_from_path(dat, p)$path, p)
		expect_equal(trimws(node_from_path(dat, p)$value), "0")
		expect_s3_class(node_from_path(dat, p)$node[[1]], "XMLAbstractNode")
	}
})
