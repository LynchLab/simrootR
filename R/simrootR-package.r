#' simrootR: Input and output wrangling for OpenSimRoot models
#'
#' Tools to ease the process of setting up, running, and handling results from
#' the structural-functional plant root model OpenSimRoot (Postma et al 2017;
#' doi:10.1111/nph.14641).
#' 
#' Developed and tested against the 2017 release of OpenSimRoot; many functions may also work with earlier (pre-"Open") SimRoot versions, but this is not tested or supported.
#' 
#' @seealso [https://rootmodels.gitlab.io](https://rootmodels.gitlab.io)
#'
#' @name simrootR
#' @docType package
NULL
