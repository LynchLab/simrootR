# simrootR: Input and output wrangling for OpenSimRoot models

## Current contents

Some input-modification functions that CKB whipped up for the LEADER test panel simulations, plus a start on documentation and tests.

## Eventual contents

A suite of R tools to make dealing with OpenSimRoot more pleasant, probably including:

* Easy parameter lookup/alteration in existing input files
* Setup and teardown for ensemble runs of any size
* Extract values from results -- especially values buried in VTUs (e.g. root number)
* Visualizations

## Installation

```r
install_packages("devtools")
devtools::install_git("https://gitlab.com/LynchLab/simrootR.git")
library("simrootR")
```

## Usage

 TK
